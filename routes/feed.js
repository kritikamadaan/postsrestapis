const express= require('express')
const  router=express.Router();

const feedController = require('../controllers/feed')
// /feed/posts  get 
router.get('/posts',feedController.getPosts)

//POST /feed/post
router.post('/posts',
[body('title').trim().isLength({min:5}),
body('content').trim().isLength({min:5})],
feedController.createPosts)

router.get('/post/:postId',feedController.getPost);
module.exports=router;