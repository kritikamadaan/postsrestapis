const  { validationResult } =require('express-validater/check')
const  Post = require('../models/post');

exports.getPosts = (req,res,next) => {
    Post.find()
    .then(posts=>{
        res.status(200).json({
            message :"Fetched posts successfully",
            posts:posts
        });
    })
    .catch(err =>{
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err)
    })
    res.status(200).json({
        posts:[{
        _id:1,
        title:'First Post',
        content :'this is first content',
         imageUrl:'images/duck.jpg',
         creator:{
             name:'Maxmillian'
         },
         date:new Date()
        }]
    })
}
exports.createPosts = (req,res,next) =>{
    const errors = validationResult(req);
    if(!errors.isEmpty()){
//         return res.status(422).json({
//     message:'Validation failed,entered data is incorrect',
//     errors:errors.array()
// })
    const error =new Error('Validation failed,entered data is not correct')
    error.statusCode = 422;
    throw error
    }
    console.log('response',res)
    const title = req.body.title
    const content = req.body.content
    const  post = new Post({
        title:title,
        content:content,
        imageUrl:'images/duck.jpg',
        creator:{name:'Maxmillian'}
    })
    post.save().then(result=>{
        console.log(result);
        res.status(201).json({
            message:'Post created successfully',
            post:result
        })
    }).catch(err=>{
        console.log(err)
        if(!err.statusCode){
            err.statusCode = 500;
        }
        next(err)
    })
    console.log('request',req.body)
    res.status(201).json({
        message:"Posts created successfully",
        post :{
            _id: new Date().toISOString(),
            title:title,
            content:content,
            creator:{name:'Maximillian'},
            createdAt:new Date()
        }
    })
}
exports.getPost = (req,res,next) => {
    const  postId = req.params.postId ;
    Post.findById(postId)
        .then(post => {
            if(!post){
                const error= new Error('Could not find post')
                error.statusCode = 404;
                throw error;
            }
            res.status(200).json({message:"Post  fetched ",post:post})
        })
        .catch(err=>{
            if(!err.statusCode){
                err.statusCode = 500;
            }
            next(err);
        })
}