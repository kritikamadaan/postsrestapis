const mongoose= require('mongoose');
const schema= mongoose.Schema;

const postSchema = new Schema({
    title:{
        type:String,
        required:true
    },
    imageUrl:{
        type:String,
        required:true
    },
    content:{
        type:String,
        required:true
    },
    creator:{
        type:Object,
        required:String
    }
},{timeStamps:true})

module.exports = mongoose.model('Post',postSchema)